#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <DHT.h>


#define DHTTYPE DHT11

const char* ssid = "hsNCE";                         //nome da rede wifi
const char* password = NULL;                        //senha (null se não houver senha)
const char* mqtt_server = "146.164.41.86";          //ip do broker
const char* topic = "Kura/client/broker/medicao";   //nome do tópico

// DHT Sensor
const int DHTPin = 5;       //pino d1
const int BlueLedPin = 4;   //pino d2
const int YellowLedPin = 0; //pino d3
const int RedLedPin = 12;   //pino d6
const int GreenLedPin = 13; //pino d7

// Initialize DHT sensor.
DHT dht(DHTPin, DHTTYPE);

// Temporary variables
static char celsiusTemp[7];
static char fahrenheitTemp[7];

WiFiClient espClient;
PubSubClient client(espClient);
long lastMsg = 0;
char msg[50];
int value = 0;
int clientId = 1;

void setup_wifi() {

  delay(10);
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    digitalWrite(RedLedPin, HIGH);
    Serial.print(".");
    delay(200);
    digitalWrite(RedLedPin, LOW);
    delay(200);
  }
  digitalWrite(RedLedPin, HIGH);

  randomSeed(micros());

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();

}

void reconnect() {
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    String strClientId = "ESP8266-1";
    
    if (client.connect(strClientId.c_str())) {
      Serial.println("connected");
      //subscribe
      client.subscribe(topic);
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // aguarda 5 segundos para nova tentativa
      delay(5000);
    }
  }
}

void setup() {
  pinMode(BlueLedPin, OUTPUT);
  pinMode(RedLedPin, OUTPUT);
  pinMode(GreenLedPin, OUTPUT);
  Serial.begin(115200);
  
  setup_wifi();
  
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);

  dht.begin();
}

void loop() {

  if (!client.connected()) {
    reconnect();
  }
  client.loop();
  
  digitalWrite(GreenLedPin, HIGH);
  
  digitalWrite(BlueLedPin, LOW);
  
  long now = millis();
  if (now - lastMsg > 2000) {
    lastMsg = now;

    //leitura de umidade e temperatura
    float h = dht.readHumidity();
    float t = dht.readTemperature();

    if (!isnan(h) && !isnan(t)){  

      snprintf (msg, 100, "ESP8266-1;%.1f;%.1f", t, h);
      client.publish(topic, msg);
      
      Serial.print("Publish message ");
      Serial.println(msg);
      
      digitalWrite(BlueLedPin, HIGH);
      delay(500);
    }
    

  }
}

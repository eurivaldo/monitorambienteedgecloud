package br.ufrj.pesc.labiot.trabalhofinal;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NoSuchElementException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.eclipse.kura.cloudconnection.listener.CloudConnectionListener;
import org.eclipse.kura.cloudconnection.listener.CloudDeliveryListener;
import org.eclipse.kura.cloudconnection.message.KuraMessage;
import org.eclipse.kura.cloudconnection.publisher.CloudPublisher;
import org.eclipse.kura.cloudconnection.subscriber.CloudSubscriber;
import org.eclipse.kura.cloudconnection.subscriber.listener.CloudSubscriberListener;
import org.eclipse.kura.configuration.ConfigurableComponent;
import org.eclipse.kura.gpio.GPIOService;
import org.eclipse.kura.gpio.KuraClosedDeviceException;
import org.eclipse.kura.gpio.KuraGPIODeviceException;

import org.eclipse.kura.gpio.KuraUnavailableDeviceException;
import org.eclipse.kura.message.KuraPayload;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.ComponentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



import br.ufrj.pesc.labiot.trabalhofinal.hardware.LedAdapter;
import br.ufrj.pesc.labiot.trabalhofinal.hardware.SensorAdapter;


/*
 * Classe que representa o bundle de monitoramento de temperatura e umidade.
 * Implementa as interfaces necessarias para a comunica��o com a cloud e herda de ConfigurableComponent
 * para que permita a configura��o das propriedades pelo usu�rio na interface do Kura
 * */
public class MonitorAmbiente implements ConfigurableComponent, CloudConnectionListener, CloudDeliveryListener, CloudSubscriberListener {
  
    private static final Logger logger = LoggerFactory.getLogger(MonitorAmbiente.class);
     
    private static final String TEMP_THRESHOLD_NAME = "temperature.threshold";
    private static final String HUM_THRESHOLD_NAME = "humidity.threshold";
    private static final String PUBLISH_RATE_PROP_NAME = "publish.rate";

    private final ScheduledExecutorService worker;
    private ScheduledFuture<?> handle; 

    private float temperatureThreshold;
    private float humidityThreshold;
    private int publishRate;
    
    private Map<String, Object> properties;
		
	private static Map<String, Collection<InstantMeasure>> localDB = new HashMap<String, Collection<InstantMeasure>>();
    
    private CloudPublisher cloudPublisher;
    private CloudSubscriber cloudSubscriber;
   
    // ----------------------------------------------------------------
    //
    // Depend�ncias
    //
    // ----------------------------------------------------------------

    public MonitorAmbiente() {
        super();    
        this.worker = Executors.newSingleThreadScheduledExecutor();
		
    }

    public void setCloudPublisher(CloudPublisher cloudPublisher) {
        this.cloudPublisher = cloudPublisher;
        this.cloudPublisher.registerCloudConnectionListener(MonitorAmbiente.this);
        this.cloudPublisher.registerCloudDeliveryListener(MonitorAmbiente.this);
    }

    public void unsetCloudPublisher(CloudPublisher cloudPublisher) {
        this.cloudPublisher.unregisterCloudConnectionListener(MonitorAmbiente.this);
        this.cloudPublisher.unregisterCloudDeliveryListener(MonitorAmbiente.this);
        this.cloudPublisher = null;
    }
    
    public void setCloudSubscriber(CloudSubscriber cloudSubscriber) {
        this.cloudSubscriber = cloudSubscriber;
        this.cloudSubscriber.registerCloudConnectionListener(MonitorAmbiente.this);
        this.cloudSubscriber.registerCloudSubscriberListener(MonitorAmbiente.this);
    }

    public void unsetCloudSubscriber(CloudSubscriber cloudSubscriber) {
        this.cloudSubscriber.unregisterCloudConnectionListener(MonitorAmbiente.this);
        this.cloudSubscriber.unregisterCloudSubscriberListener(MonitorAmbiente.this);
        this.cloudSubscriber = null;
    }

    // ----------------------------------------------------------------
    //
    // APIs de ativa��o
    //
    // ----------------------------------------------------------------

    /* Metodo de ativa��o do bundle
     * Respons�vel por fazer o setup do ambiente, como reset dos LEDs
     * */
    protected void activate(ComponentContext componentContext, Map<String, Object> properties) throws InterruptedException, KuraUnavailableDeviceException, KuraClosedDeviceException, IOException, KuraGPIODeviceException {
        logger.info("Activating MonitorAmbiente...");
        
        this.properties = properties;
        for (Entry<String, Object> property : properties.entrySet()) {
            logger.info("Update - {}: {}", property.getKey(), property.getValue());
        }
        
        try {
            doUpdate(false);
        } catch (Exception e) {
            logger.error("Error during component activation", e);
            throw new ComponentException(e);
        }
        logger.info("Activating MonitorAmbiente... Done.");
    }

    /* Desativa��o do bundle*/
    protected void deactivate(ComponentContext componentContext) {
        logger.debug("Deactivating MonitorAmbiente...");

        this.worker.shutdown();

        logger.debug("Deactivating MonitorAmbiente... Done.");
    }

    /* Atualiza��o do bundle.
     * � chamado sempre que um usu�rio atualiza as configura��es de temperatura e umidade
     * */
    public void updated(Map<String, Object> properties) {
        logger.info("Updated MonitorAmbiente...");

        // Armazena as propriedades alteradas pelo usu�rio
        this.properties = properties;
        for (Entry<String, Object> property : properties.entrySet()) {
            logger.info("Update - {}: {}", property.getKey(), property.getValue());
        }

        doUpdate(true);
        logger.info("Updated MonitorAmbiente... Done.");
    }

    // ----------------------------------------------------------------
    //
    // Callback de Methods da Cloud
    //
    // ----------------------------------------------------------------

    @Override
    public void onConnectionLost() {
        // TODO Auto-generated method stub

    }

    @Override
    public void onConnectionEstablished() {
        // TODO Auto-generated method stub

    }

    @Override
    public void onMessageConfirmed(String messageId) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onDisconnected() {
        // TODO Auto-generated method stub

    }

    // ----------------------------------------------------------------
    //
    // Methodos privados
    //
    // ----------------------------------------------------------------

    /**
     * Metodo respons�vel pela atualiza��o dos valores das propriedades do bundle
     */
    private void doUpdate(boolean onUpdate) 
	{       
        if (this.handle != null) {
            this.handle.cancel(true);
        }

        if (!this.properties.containsKey(HUM_THRESHOLD_NAME)
        		|| !this.properties.containsKey(TEMP_THRESHOLD_NAME)
                || !this.properties.containsKey(PUBLISH_RATE_PROP_NAME)) {
            logger.info(
                    "Update MonitorAmbiente - Sem propriedades obrigat�rias preenchidas");
            return;
        }
       
        this.temperatureThreshold = (Float) this.properties.get(TEMP_THRESHOLD_NAME);
        this.humidityThreshold = (Float) this.properties.get(HUM_THRESHOLD_NAME);
        this.publishRate = (Integer) this.properties.get(PUBLISH_RATE_PROP_NAME);
			
		if (this.cloudPublisher == null) {
			logger.info("N�o foi selecionado um publisher. A medi��o n�o foi iniciada");
			return;
		}        
		
		if (this.cloudSubscriber == null) {
			logger.info("N�o foi selecionado um subscriber. A medi��o n�o foi iniciada");
			return;
		}
			
			
    }     
	
	 /**
     * Metodo respons�vel pelo recebimento de mensagens do cliente e tratamento para envio ao Kapua
     */	 
	@Override
	public void onMessageArrived(KuraMessage arg0) 
	{		
		String msg = new String(arg0.getPayload().getBody(), StandardCharsets.UTF_8);
		
		logger.info("Mensagem Recebida: {}", msg);				
				
		String[] tokens = msg.split(";");
		String clientId =  tokens[0];
		Float temp =  Float.parseFloat(tokens[1]);
		Float umid =  Float.parseFloat(tokens[2]);
		
		InstantMeasure currentMeasure = new InstantMeasure(temp,umid);
						
		Collection<InstantMeasure> list = localDB.get(clientId);
		if (list != null) 
		{	
			if( list.size() > 1 )
			{
				logger.info("Buscando medias");
				Float avgTemp = (float) list.stream().mapToDouble(val -> val.getTemperature()).average().orElse(0.0);
				Float avgHumid = (float) list.stream().mapToDouble(val -> val.getHumidity()).average().orElse(0.0);
			
				logger.info("avgTemp: {}", avgTemp);
				logger.info("avgHumid: {}", avgHumid);
				
				
				InstantMeasure oldestMeasure = list.stream().min(Comparator.comparing(InstantMeasure::getDate)).orElseThrow(NoSuchElementException::new);
	  			
				long diffInMillies = currentMeasure.getDate().getTime() - oldestMeasure.getDate().getTime();
				long diffInMinutes = TimeUnit.MINUTES.convert(diffInMillies, TimeUnit.MILLISECONDS);				
						
				logger.info("diffInMinutes: {}", diffInMinutes);
				
				//Se a tempertura atual ficou 1 grau acima da m�dia cria mensagem de alerta, caso contr�rio envia a mensagem de resumo quando atingir a janela de 1 minuto				
				if((Math.abs(currentMeasure.getTemperature() - avgTemp) > 1) ||
				  (Math.abs(currentMeasure.getHumidity() - avgHumid) > 1))				  
				{						
					sendMessageToKapua( "alerta", temp, umid, avgTemp, avgHumid, clientId, list.size());									
				}
				else if( diffInMinutes >= publishRate)									
				{
					sendMessageToKapua( "resumo", temp, umid, avgTemp, avgHumid, clientId, list.size());
					list.clear();
				}		
			}
			else
				logger.info("CientID com poucas medi��es ainda: {}", clientId);

			
			list.add(currentMeasure);
		} 
		else 
		{
			logger.info("Inserindo novo clientID: {}", clientId);		
			//Novo clientId
			Collection<InstantMeasure> coll = new ArrayList<InstantMeasure>();
			coll.add(currentMeasure);			
			localDB.put(clientId, coll);
		}			
	}


	 /**
    * Envia o conjunto de dados como m�tricas para o Kapua, identificando o tipo de mensagem
    */	 
	private void sendMessageToKapua(String tipo, Float temp, Float umid, Float avgTemp, Float avgHumid, String clientId, int len) 	
	{
		KuraPayload payload = new KuraPayload();
		payload.addMetric("temperatureMeasurement", temp);
		payload.addMetric("humidityMeasurement", umid);                
		payload.addMetric("averageTemperature", avgTemp);
		payload.addMetric("averageHumidity", avgHumid); 
		payload.addMetric("measurements", len);
		payload.addMetric("sensorId", clientId);
					
		Map<String,Object> prop = new HashMap<String, Object>();
		prop.put("tipo", tipo);
					
		payload.setTimestamp(new Date());
		
		KuraMessage message = new KuraMessage(payload,prop);
        
        //Publica a mensagem
        try {
            this.cloudPublisher.publish(message);
            logger.info("Published message: {}", payload);
        } catch (Exception e) {
            logger.error("Cannot publish message: {}", message, e);
        }
	}
}

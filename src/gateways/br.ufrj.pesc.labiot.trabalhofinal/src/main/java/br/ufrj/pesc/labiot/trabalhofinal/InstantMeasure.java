package br.ufrj.pesc.labiot.trabalhofinal;

import java.util.Date;

/*Classe que representa uma medi��o*/
public class InstantMeasure {
	public InstantMeasure(float temperature, float humidity) {
		this.temperature = temperature;
		this.humidity = humidity;
		this.date = new Date();
	}

	public InstantMeasure(String temperature, String humidity) {
		this.temperature = Float.valueOf(temperature);
		this.humidity = Float.valueOf(humidity);
		this.date = new Date();
	}

	
	public float getTemperature() {
		return temperature;
	}

	public float getHumidity() {
		return humidity;
	}
	
	public Date getDate() {
		return date;
	}

	private float temperature;
	private float humidity;
	private Date date;

	@Override
	public boolean equals(Object arg) {
		return this.temperature == ((InstantMeasure) arg).temperature
				&& this.humidity == ((InstantMeasure) arg).humidity
				&& this.date == ((InstantMeasure) arg).date;

	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return temperature + " : " + humidity;
	}
}